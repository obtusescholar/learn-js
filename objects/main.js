// Start init function after page has loaded
window.addEventListener('load', init);

function Car(fuel, color, weight, wheels) {
    /* Base Car class */

    // Default value 4 for wheels
    if (typeof wheels === 'undefined') {
        this.wheels = 4;
    } else {
        this.wheels = wheels;
    }
    this.fuel = fuel;
    this.color = color;
    this.weight = weight;
}

function Toyota(fuel, color, weight, model, wheels) {
    /* Toyota class, inherits from Car */

    // Inherit Car Class
    Car.call(this, fuel, color, weight, wheels);
    this.brand = 'Toyota';
    this.model = model;
}

function Tesla(fuel, color, weight, model, wheels) {
    /* Tesla class, inherits from Car */

    // Inherit Car Class
    Car.call(this, fuel, color, weight, wheels);
    this.brand = 'Tesla';
    this.model = model;
}

function getCars() {
    let corolla = new Toyota('gas', 'yellow', '1100kg', 'Corolla');
    let tesla = new Tesla('electric', 'gray', '1200kg', 'Model 3');

    let cars = [corolla, tesla];
    return cars;
}

function init(e) {
    /* Initiate and show objects */

    let cars = getCars();

    for (car in cars) {
        let currentCar = cars[car];
        for (value in currentCar) {
            var mainHtml = document.getElementsByTagName('MAIN')[0];
            let newContent = document.createElement('P');
            newContent.innerHTML = value + ': ' + currentCar[value] + '<br>';

            while (newContent.firstChild) {
                mainHtml.appendChild(newContent.firstChild);
            }
        }

        let spacer = document.createElement('BR');
        mainHtml.appendChild(spacer);
    }
}
