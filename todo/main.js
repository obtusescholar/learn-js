window.addEventListener('load', init, false);

function init(e) {
    /* Initiate event listeners */

    document.getElementById('send')
        .addEventListener('click', submitForm);

    document.getElementById('reset')
        .addEventListener('click', resetForm);

    let i_item = todoIndex('h_item');
    document.getElementById('h_item')
        .addEventListener('click', function() {sortTable(i_item)});

    let i_urgency = todoIndex('h_urgency');
    document.getElementById('h_urgency')
        .addEventListener('click', function() {sortTable(i_urgency)});
}

function todoIndex(columnId) {
    /* Returns column index for column id */

    return document.getElementById(columnId).cellIndex;
}

function getTableBody() {
    /* Return todolist tbody  */

    return document.getElementById('todoList')
        .getElementsByTagName('tbody')[1];
}

function resetForm() {
    /* Empty todolist */

    getTableBody().innerHTML = "";
}

function submitForm() {
    /* Prints todo items into table */

    // get values from form
    let item = document.todoForm.item.value;
    let urgency = document.todoForm.urgency.value;

    // define input item input area
    let tableRef = getTableBody();
    let newRow = tableRef.insertRow(tableRef.rows.length);

    // input item into table
    newRow.innerHTML = '<td class="item">' + item
        + '</td><td class="urgency">' + urgency + '</td>';
}

function sortTable(n) {
    /* Does sorting for todo table when clicking headers
     *
     * Source W3School with my additions and comments */

    // initiate variables
    var table, rows, switching, i, a, b, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("todoList");
    switching = true;
    dir = "asc";

    // loop the for rows loop over and over until all items sorted
    while (switching) {

        // start by saying no more looping needed
        switching = false;

        // get table rows
        rows = table.rows;

        // loop trough data rows
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;

            // get row and next row
            a = rows[i].getElementsByTagName("TD")[n];
            b = rows[i + 1].getElementsByTagName("TD")[n];

            // if ascending or descending
            if (dir == "asc") {
                if (a.innerHTML.toLowerCase() > b.innerHTML.toLowerCase()) {

                    // exit row loop and mark row and next row for switching
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (a.innerHTML.toLowerCase() < b.innerHTML.toLowerCase()) {

                    // exit row loop and mark row and next row for switching
                    shouldSwitch = true;
                    break;
                }
            }
        }

        // has for loop found row that needs switching?
        if (shouldSwitch) {
            // move next row before current row
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

            // tell while loop to continue looping
            switching = true;

            // increment how many items switched
            switchcount ++;
        } else {
            // if no items needed switching with ascended order
            // else no more changed needed, exit loop with switching=false
            if (switchcount == 0 && dir == "asc") {

            // change direction into descended
            dir = "desc";

            // tell while loop to continue looping
            switching = true;
            }
        }
    }
}
